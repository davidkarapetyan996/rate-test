<?php

declare(strict_types=1);

namespace Modules\Rate\V1\Application\Action;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Rate\V1\Application\UseCase\Query\RateIndex\RateIndexQuery;
use Modules\Rate\V1\Domain\Handler\RateIndexHandler;
use OpenApi\Attributes as OA;

#[OA\Get(
    path: '/api/v1/rate-all',
    operationId: 'getRates',
    summary: 'Get rates',

    parameters: [
        new OA\QueryParameter(
            name: 'page',
            schema: new OA\Schema(
                type: 'int',
                example: 1
            )
        ),
        new OA\QueryParameter(
            name: 'per_page',
            schema: new OA\Schema(
                type: 'int',
                example: 10
            )
        )
    ],

    responses: [
        new OA\Response(
            response: 200,
            description: 'Success',
            content: new OA\JsonContent(
                required: ['data'],
                properties: [
                    new OA\Property(
                        property: 'data',
                        type: 'array',
                        items: new OA\Items(
                            ref: '#/components/schemas/RateDataObject',
                            description: 'RateDataObject'
                        ),
                    ),
                ]
            )
        ),
        new OA\Response(
            response: 400,
            description: 'Bad Request',
            content: new OA\JsonContent(
                ref: '#/components/schemas/BadRequestObject'
            )
        ),
        new OA\Response(
            response: 422,
            description: 'Validation failed',
            content: new OA\JsonContent(
                ref: '#/components/schemas/ValidationFailedObject'
            )
        ),
        new OA\Response(
            response: 500,
            description: 'Server Error',
            content: new OA\JsonContent(
                ref: '#/components/schemas/ServerErrorObject'
            )
        )
    ]
)]
final class RateIndexAction extends Controller
{
    public function run(RateIndexQuery $request, RateIndexHandler $handler): JsonResponse
    {
        return new JsonResponse(data: $handler->run(dto: $request->getDto()), status: 200);
    }
}
