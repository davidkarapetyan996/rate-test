<?php

declare(strict_types=1);

namespace Modules\Rate\V1\Application\UseCase\Query\RateByCurrencyView;

final class RateByCurrencyViewDTO
{
    public function __construct(
        protected readonly string $currency
    )
    {
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }
}
