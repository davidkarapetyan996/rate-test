<?php

declare(strict_types=1);

namespace Modules\Rate\V1\Application\UseCase\Query\RateByCurrencyView;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

final class RateByCurrencyViewQuery extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function prepareForValidation(): void
    {
        $this->merge(input: ['currency' => $this->input(key: 'currency')]);
    }


    public function rules(): array
    {
        return [
            'currency' => 'required|string|regex:/^[a-zA-Z]+$/'
        ];
    }

    public function messages(): array
    {
        return [
            'currency.regex' => 'The :attribute field must contain only letters.',
        ];
    }

    public function getDto(): RateByCurrencyViewDTO
    {
        return new RateByCurrencyViewDTO(
            currency: strtoupper($this->get(key: 'currency'))
        );
    }

    protected function failedValidation(Validator $validator): HttpResponseException
    {
        throw new HttpResponseException(
            response: response()->json(
                data: [
                    'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'error' => $validator->errors(),
                    'message' => 'Validation failed',
                ],
                status: Response::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
