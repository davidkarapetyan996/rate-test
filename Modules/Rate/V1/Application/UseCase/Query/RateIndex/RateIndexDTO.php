<?php

declare(strict_types=1);

namespace Modules\Rate\V1\Application\UseCase\Query\RateIndex;

final class RateIndexDTO
{
    public function __construct(
        protected readonly int $page,
        protected readonly int $perPage
    )
    {
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getPerPage(): int
    {
        return $this->perPage;
    }
}
