<?php

declare(strict_types=1);

namespace Modules\Rate\V1\Application\UseCase\Query\RateIndex;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

final class RateIndexQuery extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function prepareForValidation(): void
    {
        $this->merge(input: ['page' => $this->input(key: 'page', default: 1)]);
        $this->merge(input: ['per_page' => $this->input(key: 'per_page', default: 10)]);
    }

    public function rules(): array
    {
        return [
            'page' => 'integer',
            'per_page' => 'integer'
        ];
    }

    public function getDto(): RateIndexDTO
    {
        return new RateIndexDTO(
            page: (int) $this->get('page'),
            perPage: (int) $this->get('per_page')
        );
    }

    protected function failedValidation(Validator $validator): HttpResponseException
    {
        throw new HttpResponseException(
            response: response()->json(
                data: [
                    'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'error' => $validator->errors(),
                    'message' => 'Validation failed',
                ],
                status: Response::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
