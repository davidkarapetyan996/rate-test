<?php

namespace Modules\Rate\V1\Domain\Entity;

use Illuminate\Database\Eloquent\Model;
use OpenApi\Attributes as OA;

#[OA\Schema(
    schema: 'RateDataObject',
    required: [
        'char_code',
        'name',
        'value',
        'rate_value',
    ],
    properties: [
        new OA\Property(
            property: 'currency',
            type: 'string',
            example: 'USD',
        ),
        new OA\Property(
            property: 'name',
            type: 'string',
            example: 'Доллар США'
        ),
        new OA\Property(
            property: 'value',
            type: 'float',
            example: '91,6359'
        ),
        new OA\Property(
            property: 'rate_value',
            type: 'float',
            example: '91,6359'
        )
    ],
    type: 'object'
)]

/**
 * @property string $char_code
 * @property string $name
 * @property float $value
 * @property float $rate_value
 */
class Rate extends Model
{
    protected $fillable = [
        'char_code',
        'name',
        'value',
        'rate_value'
    ];
}
