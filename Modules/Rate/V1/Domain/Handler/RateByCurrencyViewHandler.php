<?php

declare(strict_types=1);

namespace Modules\Rate\V1\Domain\Handler;

use Modules\Rate\V1\Domain\View\RateByCurrencyView;
use Modules\Rate\V1\Domain\Repository\IRateRepository;
use Modules\Rate\V1\Application\UseCase\Query\RateByCurrencyView\RateByCurrencyViewDTO;

final class RateByCurrencyViewHandler
{
    public function __construct(
        protected readonly IRateRepository $repository,
        protected readonly RateByCurrencyView $view
    )
    {
    }

    public function run(RateByCurrencyViewDTO $dto): array
    {
        $rateByCurrency = $this->repository->getRateByCurrency(currency: $dto->getCurrency());

        return $this->view->map(model: $rateByCurrency);
    }
}
