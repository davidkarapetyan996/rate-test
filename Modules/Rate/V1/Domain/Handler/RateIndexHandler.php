<?php

namespace Modules\Rate\V1\Domain\Handler;

use Modules\Rate\V1\Application\UseCase\Query\RateIndex\RateIndexDTO;
use Modules\Rate\V1\Domain\Repository\IRateRepository;
use Modules\Rate\V1\Domain\View\RateIndexView;

final class RateIndexHandler
{
    public function __construct(
        protected readonly IRateRepository $repository,
        protected readonly RateIndexView $view
    )
    {
    }

    public function run(RateIndexDTO $dto): array
    {
        $rateByCurrency = $this->repository->getAll(dto: $dto);

        return $this->view->map(models: $rateByCurrency);
    }
}
