<?php

namespace Modules\Rate\V1\Domain\Repository;

use Modules\Rate\V1\Application\UseCase\Query\RateIndex\RateIndexDTO;
use Modules\Rate\V1\Domain\Entity\Rate;

interface IRateRepository
{
    public function getRateByCurrency(string $currency): ?Rate;

    public function getAll(RateIndexDTO $dto): ?array;

}
