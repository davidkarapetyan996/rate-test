<?php

declare(strict_types=1);

namespace Modules\Rate\V1\Domain\View;

use Modules\Rate\V1\Domain\Entity\Rate;

class RateByCurrencyView
{
    public function map(?Rate $model): array
    {
        return [
            'data' => [
                'char_code' => $model?->char_code,
                'name' => $model?->name,
                'value' => $model?->value,
                'rate_value' => $model?->rate_value
            ]
        ];
    }
}
