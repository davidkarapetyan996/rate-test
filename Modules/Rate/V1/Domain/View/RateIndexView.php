<?php

declare(strict_types=1);

namespace Modules\Rate\V1\Domain\View;

class RateIndexView
{
    public function map(array $models): array
    {
        $data = [];

        foreach ($models['data'] as $model) {
            $data['data'][] = [
                'char_code' => $model['char_code'],
                'name' => $model['name'],
                'value' => $model['value'],
                'rate_value' => $model['rate_value']
            ];
        }

        return $data;
    }
}
