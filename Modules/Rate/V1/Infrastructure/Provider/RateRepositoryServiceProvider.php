<?php

namespace Modules\Rate\V1\Infrastructure\Provider;

use Illuminate\Support\ServiceProvider;
use Modules\Rate\V1\Domain\Handler\RateByCurrencyViewHandler;
use Modules\Rate\V1\Domain\Repository\IRateRepository;
use Modules\Rate\V1\Infrastructure\Repository\RateRepository;

final class RateRepositoryServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(abstract: IRateRepository::class, concrete: RateRepository::class);
    }
}
