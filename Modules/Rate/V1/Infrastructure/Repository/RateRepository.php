<?php

declare(strict_types=1);

namespace Modules\Rate\V1\Infrastructure\Repository;

use Modules\Rate\V1\Domain\Entity\Rate;
use Modules\Rate\V1\Domain\Repository\IRateRepository;
use Modules\Rate\V1\Application\UseCase\Query\RateIndex\RateIndexDTO;

final class RateRepository implements IRateRepository
{
    public function getRateByCurrency(string $currency): ?Rate
    {
        return Rate::where('char_code', $currency)->first();
    }

    public function getAll(RateIndexDTO $dto): array
    {
        return Rate::query()
            ->paginate(perPage: $dto->getPerPage(), page:$dto->getPage())
            ->toArray();
    }
}
