<?php

declare(strict_types=1);

namespace Modules\RateApi\V1\Application\Command;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Modules\RateApi\V1\Domain\Service\FillCurrencyRateService;

final class FillCurrencyRateCommand extends Command
{
    protected $signature = 'currency-rate:fill';

    protected $description = 'Filling currency rate';

    public function __construct(
        protected readonly FillCurrencyRateService $service
    )
    {
        parent::__construct();
    }

    /**
     * @throws Exception
     */
    public function __invoke(): void
    {
        DB::beginTransaction();

        try {
            $this->service->run();

        } catch (Exception $ex) {
            $this->error($ex->getMessage());
            Log::error(message: self::class . ': ' .$ex->getMessage());
            DB::rollback();
            exit();
        }

        DB::commit();

        $this->info(string: 'Exchange rate data has been successfully updated. ' . date(format: 'd-m-Y H:i:s'));
        Log::info(message: 'Exchange rate data has been successfully updated.');
    }

}
