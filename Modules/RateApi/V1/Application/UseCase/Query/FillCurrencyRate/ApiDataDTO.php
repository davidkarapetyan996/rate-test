<?php

declare(strict_types=1);

namespace Modules\RateApi\V1\Application\UseCase\Query\FillCurrencyRate;

final class ApiDataDTO
{
    public function __construct(
        protected readonly string $rates,
    )
    {
    }

    public function getRates(): string
    {
        return $this->rates;
    }
}
