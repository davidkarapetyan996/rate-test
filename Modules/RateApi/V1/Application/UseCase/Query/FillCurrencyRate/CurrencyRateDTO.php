<?php

declare(strict_types=1);

namespace Modules\RateApi\V1\Application\UseCase\Query\FillCurrencyRate;

final class CurrencyRateDTO
{
    public function __construct(
        protected readonly array $rates,
    )
    {
    }

    public function getRates(): array
    {
        return $this->rates;
    }
}
