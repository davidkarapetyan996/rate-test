<?php

declare(strict_types=1);

namespace Modules\RateApi\V1\Domain\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $char_code
 * @property string $name
 * @property float $value
 * @property float $rate_value
 */
class Rate extends Model
{
    protected $fillable = [
        'char_code',
        'name',
        'value',
        'rate_value'
    ];
}
