<?php

declare(strict_types=1);

namespace Modules\RateApi\V1\Domain\Handler;

use Exception;
use Modules\RateApi\V1\Application\UseCase\Query\FillCurrencyRate\CurrencyRateDTO;
use Modules\RateApi\V1\Domain\Repository\IRateApiRepository;

final class FillCurrencyRateHandler
{
    protected array $data;

    public function __construct(
        protected readonly IRateApiRepository $repository,
    )
    {
    }

    /**
     * @throws Exception
     */
    public function run(CurrencyRateDTO $rates): void
    {
        if (!array_key_exists('Valute', $rates->getRates())) {
            throw new Exception('Invalid data');
        }

        foreach ($rates->getRates()['Valute'] as $rate) {
            $this->data[] = [
                'char_code' => (string)$rate['CharCode'],
                'name' => (string)$rate['Name'],
                'value' => (float)$rate['Value'],
                'rate_value' => (float)$rate['VunitRate'],
            ];
        }

        $this->repository->truncate();
        $this->repository->insert($this->data);
    }
}
