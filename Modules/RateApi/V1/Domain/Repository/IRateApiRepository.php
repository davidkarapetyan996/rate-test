<?php

declare(strict_types=1);

namespace Modules\RateApi\V1\Domain\Repository;

interface IRateApiRepository
{
    public function insert(array $rates): void;
    public function truncate(): void;
}
