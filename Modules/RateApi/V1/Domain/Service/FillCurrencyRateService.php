<?php

declare(strict_types=1);

namespace Modules\RateApi\V1\Domain\Service;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Modules\RateApi\V1\Application\UseCase\Query\FillCurrencyRate\ApiDataDTO;
use Modules\RateApi\V1\Domain\Handler\FillCurrencyRateHandler;
use Modules\RateApi\V1\Domain\Validation\ApiDataValidate;
use Modules\RateApi\V1\Domain\Validation\CurrencyRateValidate;

final class FillCurrencyRateService
{
    public function __construct(
        protected readonly FillCurrencyRateHandler $handler
    )
    {
    }

    /**
     * @throws Exception
     */
    public function run(): void
    {
        $url = env('RATE_API_URI');

        if (!$url) {
            throw new Exception(message: 'Please indicate external REST API');
        }

        $response = Http::get(url: $url);

        if ($response->status() !== 200) {
            throw new Exception(message: 'Something went wrong');
        }

        $validatedApiData = new ApiDataValidate(rates: $response->body());
        $rates = $this->toArray(apiData: $validatedApiData->getDto());
        $validatedCurrencyRate = new CurrencyRateValidate(rates: $rates);

        $this->handler->run(rates: $validatedCurrencyRate->getDto());
    }


    private function toArray(ApiDataDTO $apiData): array
    {
        $xml = simplexml_load_string(data: $apiData->getRates(), options:  LIBXML_NOCDATA);

        return json_decode(json: json_encode(value: $xml), associative: TRUE);
    }
}
