<?php

declare(strict_types=1);

namespace Modules\RateApi\V1\Domain\Validation;

use Illuminate\Support\Facades\Validator;
use Modules\RateApi\V1\Application\UseCase\Query\FillCurrencyRate\ApiDataDTO;
use Exception;

final class ApiDataValidate
{
    /**
     * @throws Exception
     */
    public function __construct(
        private readonly string $rates
    )
    {
        $this->validation();
    }


    private function rules(): array
    {
        return [
            'rate' => 'required|string|valid_xml'
        ];
    }

    private function validationData(): array
    {
        return [
            'rate' => $this->rates
        ];
    }

    /**
     * @throws Exception
     */
    private function validation(): void
    {
        $validator = Validator::make(data: $this->validationData(), rules: $this->rules());

        if ($validator->fails()) {
            throw new Exception(message: $validator->errors()->toJson());
        }
    }

    public function getDto(): ApiDataDTO
    {
        return new ApiDataDTO(rates: $this->rates);
    }
}
