<?php

declare(strict_types=1);

namespace Modules\RateApi\V1\Domain\Validation;

use Illuminate\Support\Facades\Validator;
use Modules\RateApi\V1\Application\UseCase\Query\FillCurrencyRate\CurrencyRateDTO;
use Exception;

final class CurrencyRateValidate
{
    /**
     * @throws Exception
     */
    public function __construct(
        private readonly array $rates
    )
    {
        $this->validation();
    }


    private function rules(): array
    {
        return [
            'rate' => 'required|array',
            'rate.Valute' => 'required|array',
            'rate.Valute.*.CharCode' => 'required|string',
            'rate.Valute.*.Name' => 'required|string',
            'rate.Valute.*.Value' => 'required|string',
            'rate.Valute.*.VunitRate' => 'required|string',
        ];
    }

    private function validationData(): array
    {
        return [
            'rate' => $this->rates
        ];
    }

    /**
     * @throws Exception
     */
    private function validation(): void
    {
        $validator = Validator::make(data: $this->validationData(), rules: $this->rules());

        if ($validator->fails()) {
            throw new Exception(message: $validator->errors()->toJson());
        }
    }

    public function getDto(): CurrencyRateDTO
    {
        return new CurrencyRateDTO(rates: $this->rates);
    }
}
