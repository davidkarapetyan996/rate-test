<?php

declare(strict_types=1);

namespace Modules\RateApi\V1\Infrastructure\Provider;

use Illuminate\Support\ServiceProvider;
use Modules\RateApi\V1\Domain\Repository\IRateApiRepository;
use Modules\RateApi\V1\Infrastructure\Repository\RateApiRepository;

final class RateApiRepositoryServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(abstract: IRateApiRepository::class, concrete: RateApiRepository::class);
    }
}
