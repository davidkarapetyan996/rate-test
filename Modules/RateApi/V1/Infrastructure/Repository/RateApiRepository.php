<?php

declare(strict_types=1);

namespace Modules\RateApi\V1\Infrastructure\Repository;

use Modules\RateApi\V1\Domain\Entity\Rate;
use Modules\RateApi\V1\Domain\Repository\IRateApiRepository;

final class RateApiRepository implements IRateApiRepository
{
    public function insert(array $rates): void
    {
        Rate::query()->insert(values: $rates);
    }

    public function truncate(): void
    {
        Rate::query()->truncate();
    }

}
