<?php

declare(strict_types=1);

namespace Modules\Swagger\V1\Infrastructure\ResponseSchema;

use OpenApi\Attributes as OA;

#[OA\Schema(
    schema: 'ServerErrorObject',
    required: ['name', 'message', 'status'],
    properties: [
        new OA\Property(
            property: 'status',
            type: 'integer',
            example: 500
        ),
        new OA\Property(
            property: 'name',
            type: 'string',
            example: 'Internal Server Error'
        ),
        new OA\Property(
            property: 'message',
            type: 'string',
            example: 'Something went wrong!'
        )
    ],
    type: 'object'
)]
interface ServerErrorObject
{
}
