<?php

declare(strict_types=1);

namespace Modules\Swagger\V1\Infrastructure\ResponseSchema;

use OpenApi\Attributes as OA;

#[OA\Schema(
    schema: 'ValidationFailedObject',
    properties: [
        new OA\Property(
            property: 'status',
            type: 'integer',
            example: 422
        ),
        new OA\Property(
            property: 'name',
            type: 'string',
            example: 'Validation failed'
        ),
        new OA\Property(
            property: 'error',
            type: 'array',
            items: new OA\Items(
                type: 'object'
            )
        )
    ],
    type: 'object'
)]
interface ValidationFailedObject
{
}
