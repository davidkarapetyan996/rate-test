# Rate Documentation

REST is a Laravel microservice that interacts with the external REST API
of the [Central Bank of Russia](http://www.cbr.ru/scripts/XML_daily.asp).
The path to the external REST API is specified via the `RATE_API_URI` parameter
in the .env file. Exchange rate data is updated daily and stored in a local database.

### Project installation

- Docker Build Command - `docker-compose up -d --build`
- Installing dependencies - `composer install`

### Start a project

- Docker launch command - `docker-compose up -d` 

### Running tests

Unit testing to check individual program modules for correct operation.
- Run unit tests - `php artisan test`

PHPStan is a tool for detecting errors and type mismatches in PHP code before it is executed.
- Run PHPStan analyse - `composer run-script phpstan`

Sniff checks code for compliance with standards, warns of potential problems and stylistic errors.
- Run sniff - `composer run-script sniff`

Run all tests
-  `composer run-script test`

### [Generate api docs](http://localhost/api/documentation#/default)

- Run l5-swagger - `php artisan l5-swagger:generate`   
