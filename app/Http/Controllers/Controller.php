<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use OpenApi\Attributes as OA;

#[OA\Info(
    version: '0.1',
    title: 'Rate API'
)]

#[OA\PathItem(
    path: '/api/v1/'
)]
class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;
}
