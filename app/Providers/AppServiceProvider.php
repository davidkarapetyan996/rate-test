<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        Validator::extend(rule: 'valid_xml', extension: function ($attribute, $value) {
            $xml = simplexml_load_string(data: $value, options: LIBXML_NOCDATA);

            return $xml !== false;
        }, message: 'The :attribute must be a valid XML string.');
    }
}
