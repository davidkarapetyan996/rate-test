<?php

declare(strict_types=1);

namespace Tests\Unit;

use Modules\Rate\V1\Application\UseCase\Query\RateByCurrencyView\RateByCurrencyViewDTO;
use Modules\Rate\V1\Domain\Entity\Rate;
use Modules\Rate\V1\Domain\Handler\RateByCurrencyViewHandler;
use Modules\Rate\V1\Domain\Repository\IRateRepository;
use Modules\Rate\V1\Domain\View\RateByCurrencyView;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class RateByCurrencyViewHandlerTest extends TestCase
{
    private MockObject|IRateRepository $repositoryMock;
    private MockObject|RateByCurrencyView $viewMock;
    private RateByCurrencyViewHandler $handler;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->repositoryMock = $this->createMock(IRateRepository::class);
        $this->viewMock = $this->createMock(RateByCurrencyView::class);

        $this->handler = new RateByCurrencyViewHandler($this->repositoryMock, $this->viewMock);
    }

    public function testRateIndexHandler(): void
    {
        $dto = new RateByCurrencyViewDTO(currency: 'USD');

        $this->repositoryMock
            ->expects($this->once())
            ->method('getRateByCurrency')
            ->with($dto->getCurrency())
            ->willReturn($this->getRepositoryResult());

        $this->viewMock
            ->expects($this->once())
            ->method('map')
            ->with($this->getRepositoryResult())
            ->willReturn($this->getResult());

        $result = $this->handler->run($dto);

        $this->assertEquals($this->getResult(), $result);
    }

    private function getRepositoryResult(): Rate
    {
        $model = new Rate();

        $model->setRawAttributes([
            'char_code' => 'USD',
            'name' => 'Доллар США',
            'value' => '90.1',
            'rate_value' => '90.1',
        ]);

        return $model;
    }

    private function getResult(): array
    {
        return [
            'data' => $this->getRepositoryResult(),
        ];
    }
}
