<?php

declare(strict_types=1);

namespace Tests\Unit;

use Modules\Rate\V1\Application\UseCase\Query\RateIndex\RateIndexDTO;
use Modules\Rate\V1\Domain\Handler\RateIndexHandler;
use Modules\Rate\V1\Domain\Repository\IRateRepository;
use Modules\Rate\V1\Domain\View\RateIndexView;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class RateIndexHandlerTest extends TestCase
{
    private MockObject|IRateRepository $repositoryMock;
    private MockObject|RateIndexView $viewMock;
    private RateIndexHandler $handler;

    private int $page = 1;
    private int $perPage = 10;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->repositoryMock = $this->createMock(IRateRepository::class);
        $this->viewMock = $this->createMock(RateIndexView::class);

        $this->handler = new RateIndexHandler($this->repositoryMock, $this->viewMock);
    }

    public function testRateIndexHandler(): void
    {
        $dto = new RateIndexDTO(page: $this->page, perPage: $this->perPage);

        $this->repositoryMock
            ->expects($this->once())
            ->method('getAll')
            ->with($dto)
            ->willReturn([$this->getRepositoryResult()]);

        $this->viewMock
            ->expects($this->once())
            ->method('map')
            ->with([$this->getRepositoryResult()])
            ->willReturn($this->getResult());

        $result = $this->handler->run($dto);

        $this->assertEquals($this->getResult(), $result);
    }

    private function getRepositoryResult(): array
    {
        return [
            [
                'char_code' => 'AUD',
                'name' => 'Австралийский доллар',
                'value' => '60',
                'rate_value' => '60',
            ],
            [
                'char_code' => 'USD',
                'name' => 'Доллар США',
                'value' => '90.1',
                'rate_value' => '90.1',
            ],
        ];
    }

    private function getResult(): array
    {
        return [
            'data' => $this->getRepositoryResult(),
        ];
    }
}
